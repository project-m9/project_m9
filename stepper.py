#author: Cathryn Bruijnis, Mitchel Doets, Gijs Hurkmans, Esther de Jong, Luuk Rensen
#organisation: University of Twente
#date: 22-10-2021

#import
from button3 import button
from stage_perform import perform
import time
from str_to_int import cstr

class Stepper(object):
    def __init__(self):
        self.timer_num = 2 #timer type
        self.freq = 300 #ticker frequency
        self.starting_stage = 1 #stage at the start of the stepper
        self.bttn = button(self.timer_num, self.freq, self.starting_stage) #add class button to variable

    def start(self):
        #open and read rotation.txt
        rot = open("rotation.txt", "r")
        rd = rot.read()
        rot.close()

        #convert string rd to integer num
        num=cstr(rd).n()

        #if stepper is not in neutral position, fix that
        if num!=0:
            perform(0,0,0-num).action()
            time.sleep(5)

        
        self.bttn.start() #start ticker
    
    def stop(self):
        self.bttn.stop()

