from motor import MotorState
from biorobotics import SerialPC, Encoder
from pin_definitions import Pins
from state import State
from Leds import Leds
from controller import PID
from machine import Pin
from micro_button import MicroSwitches
from stepper import Stepper

class StateFunction(object):
    def __init__(self, robot_state, sensor_state, ticker_frequency):
        self.robot_state = robot_state
        self.sensor_state = sensor_state
        self.leds = Leds()
        self.motor_1 = MotorState(ticker_frequency,1)
        self.motor_2 = MotorState(ticker_frequency,2)
        self.microswitches = MicroSwitches()
        self.stepper = Stepper()
        self.pc = SerialPC(1)
        self.pid = PID(ticker_frequency)
        self.Encoder1A = Pin(Pins.MOTOR1_ENC_A, Pin.IN)
        self.Encoder1B = Pin(Pins.MOTOR1_ENC_B, Pin.IN)
        self.Encoder2A = Pin(Pins.MOTOR2_ENC_A, Pin.IN)
        self.Encoder2B = Pin(Pins.MOTOR2_ENC_B, Pin.IN)
        self.encoder1 = Encoder(self.Encoder1A, self.Encoder1B)
        self.encoder2 = Encoder(self.Encoder2A, self.Encoder2B)
        self.stage = 0

        self.callbacks = {
            State.SAFE: self.safe,
            State.CALIBRATION: self.calibration,
            State.READ: self.read,
            State.ON: self.on,
            State.ENDEFFECTOR: self.end_effector}
        return
        
    def safe(self):
        # Entry actions
        if self.robot_state.is_changed():
            self.robot_state.set(State.SAFE)
            self.leds.yellow.off()
            self.leds.red.on()
            self.leds.green.off()
            print('SAFE state')
        
        # Action
        self.motor_1.write(0.0)
        self.motor_2.write(0.0)

        # State guard
        if self.sensor_state.switch_value:
            #self.robot_state.set(State.READ)
            if self.stage == 0:
                self.robot_state.set(State.CALIBRATION)
            if self.stage == 1:
                self.robot_state.set(State.READ)
        return

    def read(self):
        # Entry Actions
        if self.robot_state.is_changed():
            self.robot_state.set(State.READ)
            self.leds.yellow.on()
            self.leds.red.on()
            self.leds.green.on()
            print('READ state')

        # Action
        #print('pot1 = ' + str(self.sensor_state.value_Potmeter1))
        #print('pot2 = ' + str(self.sensor_state.value_Potmeter2))
       
        # State guard
        if self.sensor_state.switch_value:
            self.robot_state.set(State.ON)

        return

    def on(self):
        # Entry actions
        if self.robot_state.is_changed():
            self.robot_state.set(State.ON)
            self.leds.yellow.on()
            self.leds.red.off()
            self.leds.green.on()
            print('ON state')

        """# Action
        self.counts1 = -self.encoder1.counter()
        self.counts2 = self.encoder2.counter()
        #print('potmeter 1 = ' + str(self.sensor_state.value_Potmeter1))
        #print('encoder1_counter = '+ str(self.counts1))
        #print('encoder_2 = ' + str(self.counts2))
        #print('potmeter 2 = ' + str(self.sensor_state.value_Potmeter2))
        self.control_signal_motor1 = (self.pid.step(self.sensor_state.value_Potmeter1, self.counts1))
        self.control_signal_motor2 = (self.pid.step(self.sensor_state.value_Potmeter2, self.counts2))
        #print('control_signal = ' + str(self.control_signal_motor1))
        pwm_1 = (self.control_signal_motor1/8400)
        pwm_2 = (self.control_signal_motor2/8400)

        #pwm_1 = 2 * (self.sensor_state.value_Potmeter1 - 0.5)
        #print('PWM1 =' + str(pwm_1))
        #print('PWM2 =' + str(pwm_2))
        self.motor_1.write(pwm_1)
        self.motor_2.write(pwm_2)"""

        # State guard
        if self.sensor_state.switch_value:
            self.robot_state.set(State.ENDEFFECTOR)
        return
    
    def end_effector(self):
        # entry action
        if self.robot_state.is_changed():
            self.robot_state.set(State.ENDEFFECTOR)
            print('End-effector State')
            self.leds.yellow.on()
            self.leds.red.off()
            self.leds.green.on()

        # main action
        self.stepper.start()

        # state guards with possible action        
        if self.sensor_state.switch_value:
            self.robot_state.set(State.SAFE)
            self.stepper.stop()

        return
    
    def calibration(self):
          # entry action
        if self.robot_state.is_changed():
            self.robot_state.set(State.CALIBRATION)
            print('Calibration state')
            self.leds.yellow.on()
            self.leds.red.on()
            self.leds.green.off()

        # Main action
        self.microswitches.Calibration_control()
        self.stage = 1
            
        #if self.sensor_state.switch_value:
        if self.microswitches.Microswitch_1_pressed and self.microswitches.Microswitch_2_pressed is True:
            self.robot_state.set(State.SAFE)
            
        return


    
