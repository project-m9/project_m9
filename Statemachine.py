from state import State
from Sensor import SensorState
from statefunctions import StateFunction
from biorobotics import Ticker

class StateMachine(object):

    def __init__(self,ticker_frequency):
        self.robot_state = State()
        self.sensor_state = SensorState()
        self.state_functions = StateFunction(self.robot_state, self.sensor_state, ticker_frequency)
        self.ticker = Ticker(0, ticker_frequency, self.run)
        return

    def run(self):
        # obtain latest sensor signals (EMG1, EMG2, EMG3, EMG4, motor encoders)
        self.sensor_state.update()

        # execute state function
        self.state_functions.callbacks[self.robot_state.current]()
        return

    def start(self):
        self.ticker.start()
        return

    def stop(self):
        self.ticker.stop()
        return