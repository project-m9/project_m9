#author: Cathryn Bruijnis, Mitchel Doets, Gijs Hurkmans, Esther de Jong, Luuk Rensen
#organisation: University of Twente
#date: 28-10-2021

#import
from machine import Pin
from pin_definitions import Pins
import time

#this script is used to calibrate the stepper by hand

#variables
deg=0 #enter number of degrees
rev=512/360*deg #conversion to steps (512 steps per revolution)

#designate the pins for the OUTPUT signals to variables
p1=Pin(Pins.EEN,Pin.OUT)
p2=Pin(Pins.TWEE,Pin.OUT)
p3=Pin(Pins.DRIE,Pin.OUT)
p4=Pin(Pins.VIER,Pin.OUT)

#define sequence for 1 step
seq=[ [1,0,0,0], #clockwise (cw)
[1,1,0,0],
[0,1,0,0],
[0,1,1,0],
[0,0,1,0],
[0,0,1,1],
[0,0,0,1],
[1,0,0,1]
]
nseq=[] #counterclockwise (ccw)
for test in range(8):
    nseq.append(seq[-test-1])

#define which sequence is used in which case
if rev<0: #ccw
    aseq=nseq
if rev>=0: #cw
    aseq=seq

#run sequence for i number of steps
for i in range(abs(rev)):
    for s in range(8):
        p1.value(aseq[s][0])
        p2.value(aseq[s][1])
        p3.value(aseq[s][2])
        p4.value(aseq[s][3])
        time.sleep(0.001)