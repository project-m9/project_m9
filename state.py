
class State(object):
    SAFE = 'safe'
    CALIBRATION = 'calibration'
    READ = 'read'
    ON = 'on'
    ENDEFFECTOR = 'end_effector'
    
    def __init__(self):
        self.previous = None
        self.current = self.SAFE
        return
    
    def set(self, state):
        self.previous = self.current
        self.current = state
        return
    
    def is_changed(self):
        return self.previous is not self.current