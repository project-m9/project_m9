#author: Cathryn Bruijnis, Mitchel Doets, Gijs Hurkmans, Esther de Jong, Luuk Rensen
#organisation: University of Twente
#date: 1-11-2021

class cstr(object):
    def __init__(self,input):
        self.i=input
        self.o=0
        return
    
    def n(self):
        #define the string containing a positive/negative number
        if self.i[0]=="-":
            s_i=1
            n=True
        else:
            s_i=0
            n=False
        
        #convert the string from txt-file to int
        if len(self.i)>1:    
            for i in range(s_i,len(self.i)):
                place=10**(len(self.i)-(i+1))
                digit=int(self.i[i])
                self.o+=place*digit
            if n:
                self.o=-self.o
            else:
                self.o=self.o
        else:
            try:
                self.o=int(self.i)
            except:
                self.o=0
        return self.o