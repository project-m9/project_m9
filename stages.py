#author: Cathryn Bruijnis, Mitchel Doets, Gijs Hurkmans, Esther de Jong, Luuk Rensen
#organisation: University of Twente
#date: 22-10-2021

#define steps per stage (+=cw, -=ccw)
class stage(object):
    cero = 0
    uno = -256
    dos = -128
    tres = 64
    cuatro = 64 
    cinco = 178 
    seis = 78 