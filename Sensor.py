from pin_definitions import Pins
from machine import Pin
from biorobotics import AnalogIn
from Switch import BlueSwitch
from controller import PID

class SensorState(object):
    def __init__(self):
        self.Potmeter1 = Pin(Pins.POTMETER1, Pin.IN)
        self.Potmeter2 = Pin(Pins.POTMETER2, Pin.IN)
        self.Blue_switch = BlueSwitch()

        return
    
    def update(self):
        # Analog signal reading potmeter 1
        a0 = AnalogIn(Pins.POTMETER1)
        self.value_Potmeter1 = round(a0.read()*8400)
        # Analog signal reading potmeter 2
        a1 = AnalogIn(Pins.POTMETER2)
        self.value_Potmeter2 = round(a1.read()*8400)
        # Call switch value
        self.switch_value = self.Blue_switch.value()
        
        return