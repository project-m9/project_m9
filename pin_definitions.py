# Definitions of which pins will be used

class Pins(object):
    NUCLEO_BLUE_BUTTON = 'C13'
    BR_BUTTON1 = ''
    POTMETER1 = 'A0'
    POTMETER2 = 'A1'

    # EMG
    #EMG_Ch1 = 'A0'
    #EMG_Ch2 = 'A1'
    #EMG_Ch3 = 'A2'
    #EMG_Ch4 = 'A3'

    # Motoren
    MOTOR1_DIRECTION = 'D7'
    MOTOR1_PWM = 'D6'
    MOTOR1_ENC_A = 'D0'
    MOTOR1_ENC_B = 'D1'
    MOTOR2_DIRECTION = 'D4'
    MOTOR2_PWM = 'D5'
    # Encoders
    MOTOR2_ENC_A = 'D11'
    MOTOR2_ENC_B = 'D12'
    # End-effector buttons
    FWB = 'D2' # Forward button
    BWB = 'D3' # Backward button
    
    # Signals to the inputs on the uln2003 (stepper driver) in order
    EEN = 'D8' 
    TWEE = 'D9'
    DRIE = 'D10'
    VIER = 'D13'
    
    # Microswitches
    MICRO_SWITCH_1 = 'D68'
    MICRO_SWITCH_2 = 'D31'
