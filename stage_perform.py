#author: Cathryn Bruijnis, Mitchel Doets, Gijs Hurkmans, Esther de Jong, Luuk Rensen
#organisation: University of Twente
#date: 28-10-2021

#import
from machine import Pin
from pin_definitions import Pins
import time
from stages import stage
from str_to_int import cstr

#this class defines what signals to send to the stepper
class perform(object):
    def __init__(self,n,b,p=0):
        #designate the pins for the OUTPUT-signals to variables
        self.p1=Pin(Pins.EEN,Pin.OUT)
        self.p2=Pin(Pins.TWEE,Pin.OUT)
        self.p3=Pin(Pins.DRIE,Pin.OUT)
        self.p4=Pin(Pins.VIER,Pin.OUT)
        self.n=n #stage
        self.b=b #backwards --> 1
        self.p=p
        return

    def action(self):
        n_a=[self.p,stage.uno,stage.dos,stage.tres,stage.cuatro,stage.cinco,stage.seis] #steps per stage
        rev=n_a[self.n] #steps for current stage
        
        #define sequence for 1 step
        seq=[ [1,0,0,0], #clockwise(cw)
        [1,1,0,0],
        [0,1,0,0],
        [0,1,1,0],
        [0,0,1,0],
        [0,0,1,1],
        [0,0,0,1],
        [1,0,0,1]
        ]
        nseq=[] #counterclockwise(ccw)
        for r in range(8):
            nseq.append(seq[-r-1])
    
        #define which sequence is used in which case
        if self.b==0: #forward
            if rev<0: #ccw
                aseq=nseq
            if rev>=0: #cw
                aseq=seq
        if self.b==1: #backward
            if rev>0: #ccw
                aseq=nseq
            if rev<=0: #cw
                aseq=seq

        #run sequence for i number of steps
        for i in range(abs(rev)):
            for s in range(8):
                self.p1.value(aseq[s][0])
                self.p2.value(aseq[s][1])
                self.p3.value(aseq[s][2])
                self.p4.value(aseq[s][3])
                time.sleep(0.001)
        
        #open and read rotation.txt
        rot = open("rotation.txt", "r")
        rd = rot.read()
        rot.close()

        #convert string to integer
        num=cstr(rd).n()

        #add number of steps to number in txt-file
        if self.b==0: #fw button
            numnum=num+rev
        else:
            numnum=num-rev

        #convert numnum to string and write to txt-file
        nr=str(numnum)
        print("angle = "+str(int(360/512*numnum))+u"\u00b0")
        with open("rotation.txt", 'w') as rot:
            rot.write(nr)
            rot.close()