
class PID(object):
    def __init__(self, ticker_frequency, p_gain=8, i_gain=1, d_gain=0):
        t_step = 1/ticker_frequency
        self.t_step = t_step
        self.p_gain = p_gain
        self.i_gain = i_gain
        self.d_gain = d_gain

        self.past_error = 0
        self.integrated_error = 0

        return

    def step(self, reference, measured):

        self.error1 = reference - measured
        #print('error = ' + str(self.error1))
        self.integrated_error += (self.error1 * self.t_step)*self.i_gain
        differential_error = ((self.error1 - self.past_error)/self.t_step)*self.d_gain
        proportional_error = self.error1*self.p_gain

        self.control_signal = proportional_error + self.integrated_error + differential_error

        return self.control_signal