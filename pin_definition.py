#author: Cathryn Bruijnis, Mitchel Doets, Gijs Hurkmans, Esther de Jong, Luuk Rensen
#organisation: University of Twente
#date: 22-10-2021

#define pins to use
class Pins(object):
    FWB = 'D2' #forward button
    BWB = 'D3' #backward button
    #signals to the inputs on the uln2003 in order
    EEN = 'D8' 
    TWEE = 'D9'
    DRIE = 'D10'
    VIER = 'D11'