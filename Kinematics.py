# Kinematics of the robot

import numpy as np
import math

# Define parameters
L1 = 0.25           # Length arm 1 [m]
L2 = 0.25           # Length arm 2 [m]
L3 = 0.40           # Length arm 3 [m]
L4 = 0.40           # Length arm 4 [m]
W = 0.20            # Width between the the motors [m]
def points(q1,q2):
    # define point P2 and P4 (in joints)
    P2 = np.array([[L1*math.cos(q1)], [L1*math.sin(q1)]])
    P4 = np.array([[W + L2*math.cos(q2)],[L2*math.sin(q2)]])

    # angles [in rad] between s2-4 (distance P2-P4) and x-axis/link 3
    theta1 = math.atan((P4[1]-P2[1])/(P4[0]-P2[0]))
    theta2 = math.acos((-(L4)**2 + (L3)**2 + ((P4[0]-P2[0])**2) + ((P4[1]-P2[1])**2))/(2*L3*(((P4[0]-P2[0])**2)+(P4[1]-P2[1])**2))**(0.5))
    theta3 = theta1 + theta2
    theta4 = theta3 - q1

    return theta4

def calcH(q1,theta4):
   
    # define H_1_to_0, rotational matrix
    H_1_to_0 = np.eye(3)
    c1 = math.cos(q1)
    s1 = math.sin(q1)
    H_1_to_0[:2,:2]=[[c1,-s1],[s1,c1]]

    # define H_2_to_1, translational matrix
    H_2_to_1 = np.eye(3)
    H_2_to_1[:2,2] = [0, L1]
    
    # define H_3_to_2, rotational matrix
    H_3_to_2 = np.eye(3)
    c2 = math.cos(theta4)
    s2 = math.sin(theta4)
    H_3_to_2[:2,:2]=[[c2, -s2],[s2, c2]]

    # define H_4_to_3, translational matrix
    H_4_to_3 = np.eye(3)
    H_4_to_3[:2,2] = [0, L3]

    # matrix multiplication of previous defined H-matrices
    H_4_to_0 = H_1_to_0 @ H_2_to_1 @ H_3_to_2 @ H_4_to_3

    H_5_to_0 = np.eye(3)
    H_5_to_0[:2,2] = [[H_4_to_0[0,2]],[H_4_to_0[1,2]]]

    P_ee = np.array([[H_5_to_0[0,2]],[H_5_to_0[1,2]]])

    return P_ee

def calcQdot(P_ee):
    
    return