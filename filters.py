
class Biquad(object):
    def __init__(self, a_param, b_param):

        if len(a_param) != 3:
            raise ValueError('Incorrect number of coefficients for a')
        if len(b_param) != 3:
            raise ValueError('Incorrect number of coefficients for b')
        
        if a_param[0] != 1:
            a0, a1, a2 = a_param
            b0, b1, b2 = b_param
            a_param = (1, a1/a0, a2/a0)
            b_param = (b0/a0, b1/a0, b2/a0)
        
        self.a_param = a_param
        self.b_param = b_param
        self.delay_register = [0.0, 0.0]
        return
    
    def step(self, sample):

        return