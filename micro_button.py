from machine import Pin
from pin_definitions import Pins
from motor import MotorState

class MicroSwitches():

    def __init__(self):
        self.Microswitch_1 = Pin(Pins.MICRO_SWITCH_1,Pin.IN, Pin.PULL_UP)
        self.Microswitch_2 = Pin(Pins.MICRO_SWITCH_2,Pin.IN, Pin.PULL_UP)
        self.Microswitch_1_previous = True
        self.Microswitch_2_previous = True
        self.Microswitch_1_pressed = False
        self.Microswitch_2_pressed = False
        self.motor_1 = MotorState(300, 1)
        self.motor_2 = MotorState(300, 2)
        return
        
    def Microswitch_1_control(self):
        current = self.Microswitch_1.value()
        if current and not self.Microswitch_1_previous:
            self.Microswitch_1_pressed = True
            if self.Microswitch_1_pressed is True:
                self.motor_1.write(0)
            print('Motor 1 is calibrated')
        elif self.Microswitch_1_pressed is False:
            self.motor_1.write(-0.2)
        self.Microswitch_1_previous = current
        
    
    def Microswitch_2_control(self):
        current2 = self.Microswitch_2.value()
        if current2 and not self.Microswitch_2_previous:
            self.Microswitch_2_pressed = True
            if self.Microswitch_2_pressed is True:
                self.motor_2.write(0)
            print('Motor 2 is calibrated')
        elif self.Microswitch_2_pressed is False:
            self.motor_2.write(0.2)
        self.Microswitch_2_previous = current2
    
    def Calibration_control(self):
        self.Microswitch_1_control()
        self.Microswitch_2_control()
