#author: Cathryn Bruijnis, Mitchel Doets, Gijs Hurkmans, Esther de Jong, Luuk Rensen
#organisation: University of Twente
#date: 28-10-2021

#import
from machine import Pin
from biorobotics import Ticker
from pin_definitions import Pins
from stage_perform import perform

#Determine if the button is pressed. If pressed, perform an action.
class button(object):
    def __init__(self, timer_num, freq, n):
        self.ticker = Ticker(timer_num, freq, self.check, True) #create ticker to check button press
        self.stage = n #define beginning stage
        return

    #check if button is pressed
    def check(self):
        self.press = 1-Pin(Pins.FWB, Pin.IN, Pin.PULL_UP).value() #fwb pressed --> press=1
        self.press2 = 1-Pin(Pins.BWB, Pin.IN, Pin.PULL_UP).value() #bwb pressed --> press2=1
        if self.press == 1:
            self.forward() #run fw action
        if self.press2 == 1:
            self.backward() #run bw action
        return

    #forward action
    def forward(self):
        self.stop() #stop ticker
        self.backwards=0 #fwb, not bwb
        print("forward button press")
        print("stage = "+str(self.stage)) #announce which stage is performed
        prfrm=perform(self.stage,self.backwards)
        prfrm.action() #initiate action
        self.update_stage() #update stage
        self.start() #start ticker
        return

    #backward action
    def backward(self):
        self.stop() #stop ticker
        self.backwards=1
        print("backward button press")
        self.update_stage()
        print(self.stage) #announce state to which will be "reversed"
        prfrm=perform(self.stage,self.backwards)
        prfrm.action()
        self.start()
        return

    #start ticker
    def start(self):
        self.ticker.start() #ticker start
        return

    #stop ticker
    def stop(self):
        self.ticker.stop() #ticker stop
        return
    
    #update aimed-for stage on button press
    def update_stage(self):
        if self.press==1: #fwb pressed --> move to next stage
            if self.stage<6:
                self.stage+=1
            elif self.stage==6:
                self.stage=1
        if self.press2==1: #bwb pressed --> move to previous stage
            if self.stage==1:
                self.stage=6
            elif self.stage>1:
                self.stage-=1
        return


    