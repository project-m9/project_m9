from pin_definitions import Pins
from machine import Pin
from biorobotics import PWM

class MotorState(object):
    def __init__(self, ticker_frequency, motor=1):

        if motor == 1:
            self.PWM1 = PWM(Pins.MOTOR1_PWM, ticker_frequency)
            self.direction = Pin(Pins.MOTOR1_DIRECTION, Pin.OUT)
        elif motor == 2:
            self.PWM1 = PWM(Pins.MOTOR2_PWM, ticker_frequency)
            self.direction = Pin(Pins.MOTOR2_DIRECTION, Pin.OUT)
        return
    
    def write(self, pwm_value):

        if pwm_value < 0:
            self.direction.value(1)
        else:
            self.direction.value(0)
        
        self.PWM1.write(abs(pwm_value))
        return


